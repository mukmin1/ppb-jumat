import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your appvvvlication.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Latihan ListView',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: ScrollingScreen(),
    );
  }
}

class ScrollingScreen extends StatelessWidget {
  final List<int> numberList = <int>[1,2,3,4,5,6,7,8,9,10];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ListView'),
      ),
      body: ListView(
        children:
        numberList.map((number) {
            return Container(
              height: 250,
              decoration: BoxDecoration(
                color: Colors.deepOrangeAccent,
                border: Border.all(
                  color: Colors.black,
                )
              ),
              child: Center(
                child: Text('$number'),
              ),
            );
        }).toList(),
      ),
    );
  }
}