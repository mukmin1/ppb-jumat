import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Biodata Diri',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: Biodata(),
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('flutter'),
      ),
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(colors: [Colors.red, Colors.blue])),
      ),
    );
  }
}

class Biodata extends StatelessWidget {
  @override
  Widget build(BuildContext) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Biodata'),
      ),
      body: SafeArea(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.all(10.0),
              height: 300.0,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('images/profil.png'),
                  fit: BoxFit.contain,
                ),
              ),
            ),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(left: 10.0),
              child: Text(
                'Mgs Djahwa H',
                style: TextStyle(
                  color: Colors.blue[800],
                  fontSize: 50.0,
                ),
              ),
            ),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.only(left: 10.0),
              child: Text(
                '031200053',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20.0,
                ),
              ),
            ),
            SizedBox(
              height: 30.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Ink(
                  decoration:
                      ShapeDecoration(color: Colors.red, shape: CircleBorder()),
                  child: IconButton(
                      color: Colors.white,
                      onPressed: () {
                        Fluttertoast.showToast(
                            msg: 'ini adalah halaman profil',
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.CENTER,
                            timeInSecForIosWeb: 1,
                            backgroundColor: Colors.red,
                            textColor: Colors.white,
                            fontSize: 16.0);
                      },
                      icon: Icon(
                        Icons.person,
                      )),
                ),
                Ink(
                  decoration: ShapeDecoration(
                      color: Colors.green, shape: CircleBorder()),
                  child: IconButton(
                      color: Colors.white,
                      onPressed: () {
                        Fluttertoast.showToast(
                            msg: "Chat saua di 03123456789",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.CENTER,
                            timeInSecForIosWeb: 1,
                            backgroundColor: Colors.green,
                            textColor: Colors.white,
                            fontSize: 16.0);
                      },
                      icon: Icon(
                        Icons.chat_bubble_rounded,
                      )),
                ),
                Ink(
                  decoration: ShapeDecoration(
                      color: Colors.blue, shape: CircleBorder()),
                  child: IconButton(
                      color: Colors.white,
                      onPressed: () {
                        Fluttertoast.showToast(
                            msg: "email saya di oke@gmail.com",
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.CENTER,
                            timeInSecForIosWeb: 1,
                            backgroundColor: Colors.blue,
                            textColor: Colors.white,
                            fontSize: 16.0);
                      },
                      icon: Icon(
                        Icons.email_rounded,
                      )),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
