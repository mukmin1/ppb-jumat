class Food {
  String name;
  String rating;
  String description;
  String price;
  String imageAsset;
  List<String> imageUrls;

  Food({
    required this.name,
    required this.rating,
    required this.description,
    required this.price,
    required this.imageAsset,
    required this.imageUrls,
  });
}

var foodList = [
  Food(
    name: 'Pempek Kapal Selam',
    rating: '5/5',
    description:
    'Pempek kapal selam adalah jenis pempek yang menyerupai kantung, dengan isian telur di dalamnya. Memangnya apa hubungan kapal selam dengan pempek? "Pempek kapal selam merupakan nama sebutan yang relatif baru. Orang Palembang dulu menyebutnya pempek telok besak," kata pemerhati sejarah Palembang, KMS H Andi Syarifuddin.',
    price: 'Rp 8.000 s/d 35.000',
    imageAsset: 'images/kapal selam.jpeg',
    imageUrls: [
      'https://asset.kompas.com/crops/6PHuCZuwCtDdOEPoUzQjkM_pXao=/0x0:1000x667/750x500/data/photo/2021/03/01/603c0e0392a4d.jpg',
      'http://4.bp.blogspot.com/-kuCtqSXJT2U/Vijr_HGVRzI/AAAAAAAABJI/C4oq6z-17F8/s1600/resep-empek-empek-kapal-selam.jpg',
      'https://images.tokopedia.net/img/cache/500-square/product-1/2015/1/21/6210493/6210493_47cd2f44-a14f-11e4-bab7-fcf74908a8c2.jpg',
    ],
  ),
  Food(
    name: 'Pempek Lenggang',
    rating: '5/5',
    description:
    'Pempek Lenggang atau juga populer dengan sebutan lenggang adalah salah satu kudapan khas yang berasal dari Kota Palembang dan Sumatra Selatan pada umumnya. Lenggang merupakan salah satu jenis varian pempek yang proses memasaknya dilakukan dengan cara dipanggang.'
        '\n\nPempek lenggang yang asli terbuat dari bahan adonan mentah pempek panggang yang terdiri dari campuran daging ikan sungai yang digiling halus, tepung sagu, air dan bumbu lalu dicampurkan bersamaan dengan telur bebek atau telur ayam di dalam wadah daun pisang berbentuk mangkuk kubus dan dipanggang diatas bara api.'
        '\n\nSebagaimana pempek pada umumnya, pempek lenggang juga dinikmati bersama saus cuko sebagai pelengkap. Selain dipanggang, pempek lenggang juga dapat disajikan dengan cara digoreng.'
        '\n\nPempek lenggang yang digoreng pada umumnya terbuat dari pempek lenjer yang dipotong kecil-kecil dan dikocok bersama telur ayam atau telur bebek lalu digoreng dengan sedikit minyak hingga adonan telur dan pempek lenjer menyatu.',
    price: 'Rp 8.000 s/d Rp 35.000',
    imageAsset: 'images/lenggang.jpeg',
    imageUrls: [
      'https://media-cdn.tripadvisor.com/media/photo-w/11/6f/e0/9d/photo2jpg.jpg',
      'https://media-cdn.tripadvisor.com/media/photo-o/06/51/a9/c0/the-making-of-pempek.jpg',
      'https://media-cdn.tripadvisor.com/media/photo-o/09/d9/85/9a/pempek-saga-sudi-mampir.jpg',
    ],
  ),
  Food(
    name: 'Laksan',
    rating: '5/5',
    description:
    'Laksan adalah makanan khas Palembang yang terbuat dari bahan baku sagu dan ikan. Laksan dibuat dalam bentuk oval dengan rasa yang hampir sama dengan pempek, tetapi disajikan dengan menggunakan kuah santan.',
    price: 'Rp 10.000 s/d Rp 35.000',
    imageAsset: 'images/laksan.jpeg',
    imageUrls: [
      'https://media-cdn.tripadvisor.com/media/photo-w/06/f7/fb/e2/lakso.jpg',
      'https://media-cdn.tripadvisor.com/media/photo-s/18/05/0f/26/laksan-palembang.jpg',
      'https://media-cdn.tripadvisor.com/media/photo-s/18/0f/74/18/laksan-pempek-soft-juicy.jpg',
    ],
  ),
  Food(
    name: 'Mie Celor',
    rating: '5/5',
    description:
    'Mi Celor adalah hidangan mi yang disajikan dalam campuran kuah santan dan kaldu udang, dicampurkan taoge dan disajikan bersama irisan telur rebus, ditaburi irisan seledri, daun bawang dan bawang goreng. Hidangan ini berasal dari kota Palembang, Sumatra Selatan, Indonesia, dan bersama dengan Pempek telah menjadi hidangan khas Palembang. Ukuran mi yang digunakan lebih besar, seperti mi Aceh atau mi Udon Jepang.',
    price: 'Rp 15.000 s/d Rp 35.000',
    imageAsset: 'images/mie celor.jpeg',
    imageUrls: [
      'https://media-cdn.tripadvisor.com/media/photo-s/05/a1/a6/11/mie-celor-26-ilir-h-syafe.jpg',
      'https://media-cdn.tripadvisor.com/media/photo-s/0c/93/cb/7a/best-mie-celor-ever.jpg',
      'https://media-cdn.tripadvisor.com/media/photo-p/15/5f/3f/a8/photo0jpg.jpg',
    ],
  ),
  Food(
    name: 'Pempek Panggang',
    rating: '4/5',
    description:
    'Pempek panggang khas Palembang memiliki cita rasa yang khas. Diolah dengan bahan baku yang cukup sederhana, bisa dengan menggabungkan ikan giling bisa juga tidak dengan ikan sesuai selera.',
    price: 'Rp 1.000 s/d Rp 5.000',
    imageAsset: 'images/pempek panggang.jpeg',
    imageUrls: [
      'https://media-cdn.tripadvisor.com/media/photo-s/09/d3/23/d9/pempek-saga-sudi-mampir.jpg',
      'https://media-cdn.tripadvisor.com/media/photo-p/14/46/10/0e/pempek-panggang.jpg',
      'https://media-cdn.tripadvisor.com/media/photo-s/07/ff/ac/7b/pempek-panggangnua-enak.jpg',
    ],
  ),
  Food(
    name: 'Martabak HAR',
    rating: '5/5',
    description:
    'Martabak HAR merupakan makanan Khas yang berasal dari India yang dibawah oleh Haji Abdul Razak. HAR sendiri merupakan singkatan/inisial nama dari pemilik tokoh martabak waralaba tersebut yaitu Haji Abdul Rozak.  Haji Abdul Rozak adalah saudagar Palembang keturunan India yang menikah dengan perempuan Palembang. Rumah makan Martabak HAR pertama berdiri sejak 7 Juli 1947 di Jalan Sudirman Palembang.',
    price: 'Rp 15.000 s/d Rp 50.000',
    imageAsset: 'images/martabak har.jpeg',
    imageUrls: [
      'https://media-cdn.tripadvisor.com/media/photo-w/0e/a2/3e/c8/img-20170305-195850-601.jpg',
      'https://media-cdn.tripadvisor.com/media/photo-w/11/45/34/42/img-20171114-192529-largejpg.jpg',
      'https://media-cdn.tripadvisor.com/media/photo-w/0c/ed/77/4c/img-20160911-161106-largejpg.jpg',
    ],
  ),
  Food(
    name: 'Pindang Patin',
    rating: '5/5',
    description:
    'Pindang merupakan makanan (lauk) khas Melayu Palembang. Pindang merupakan masakan dengan pengolahan sederhana. Pada masa lalu, aktivitas masyarakat yang tinggi, menyebabkan dorongan untuk memasak secara praktis. Pada sisi lain, Sumatra Selatan yang memiliki aliran Sungai Musi beserta anak-anak sungai lainnya, menyediakan ikan yang berlimpah.',
    price: 'Rp 20.000 s/d Rp 50.000',
    imageAsset: 'images/pindang patin.jpeg',
    imageUrls: [
      'https://media-cdn.tripadvisor.com/media/photo-o/0b/fe/2e/c7/pindang-patin-beringin.jpg',
      'https://media-cdn.tripadvisor.com/media/photo-p/10/c2/88/c9/img-20170924-115340-largejpg.jpg',
      'https://media-cdn.tripadvisor.com/media/photo-w/1d/89/38/ac/pindang-patin-is-a-fish.jpg',
    ],
  ),
];